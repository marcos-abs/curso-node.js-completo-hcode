/*
 * File: readLine.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 19:19:43
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 20:14:07
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Qual a melhor marca de Drones do Mundo?', (answer) => {
    console.log(`A melhor marca de Drones é: ${answer}`);
    switch(answer) {
        case "command":
            console.log('Ligando o Drone');
            break;
        case "takeoff":
            console.log('Decolando o Drone');
            break;
        default:
            console.log('ligue o Drone!');
    }
    rl.close();
});