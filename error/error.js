/*
 * File: error.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 14:16:15
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 14:16:15
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

// throw new Error('Ah não, deu erro? Não é possível, na minha maquina funciona! rs');

function execute() {
    executeTo();
}

function executeTo() {
    throw new Error('Ah não, deu erro? Não é possível, na minha maquina funciona! rs');
}

function init() {
    try {
        execute();
    } catch (error) {
        console.log('Temos um problema... veja o erro: ', error);
    }
}

init();
console.log('Depois do erro.');
