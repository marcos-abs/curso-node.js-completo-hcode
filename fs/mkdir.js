/*
 * File: mkdir.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Monday, 11 May 2020 12:47:32
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 11 May 2020 13:15:15
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const fs = require('fs');
const assets = ['css', 'js', 'images', 'fonts', 'lib'];

function make(dir) {
    dir.forEach((item) => {
        fs.mkdir(`projeto/assets/${item}`, {recursive: true}, (err) => {
            if(err) throw err;
            console.log('Diretório criado com sucesso:', item);
        });
    });
}

make(assets);