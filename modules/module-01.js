/*
 * File: module-01.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 15:00:53
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 16:15:44
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

console.log('Executando module-01.js');

const oculta = () => {
    console.log('Executando função oculta');
};

const executa = () => {
    console.log('Executando a função executa');
};

const welcome = `Bem vindo ao módulo module-01.js`;

module.exports = {oculta, executa, welcome};