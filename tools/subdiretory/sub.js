/*
 * File: sub.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Sunday, 10 May 2020 00:01:14
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Sunday, 10 May 2020 00:04:54
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

console.log('Diretório do Arquivo: ', __dirname);
console.log('Diretório do chamador: ', process.cwd());
