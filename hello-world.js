/*
 * File: hello-world.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 14:13:15
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 09 May 2020 14:13:16
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

console.log('HELLO WORLD');