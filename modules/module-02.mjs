/*
 * File: module-02.mjs
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 16:20:35
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 17:11:01
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

//const {executa, oculta} = require('./module-01');
//import {executa, oculta} from "./module-01";
import module01 from "./module-01.js"; //IMPORTANTE: mantenha a extensão no caso de utilizar "módulos experimentais".

/**
 * para executar este modulo experimental é necessário utilizar a seguinte sintaxe:
 * $ node --experimental-modules module-02.mjs
 */

module01.executa();

module01.oculta();
