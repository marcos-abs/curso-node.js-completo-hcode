/*
 * File: readFile.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Monday, 11 May 2020 12:24:58
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 11 May 2020 12:24:59
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const fs = require('fs');
// não bloqueante
fs.readFile('file.txt', (err, data) => {
    if(err) throw err;
    // console.log(data.toString());
});
// bloqueante
const texto = fs.readFileSync('file.txt');
console.log(`texto: ${texto}`);
