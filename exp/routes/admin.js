/*
 * File: admin.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Friday, 15 May 2020 18:31:41
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Friday, 15 May 2020 18:31:42
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
const express = require('express');
const router = express.Router();


router.get('/', (req, res) => {
    res.send('Access to Administration');
});

router.get('/:id', (req, res) => {
    res.send('Access to Administration com o Id:' + req.params.id);
});

router.post('/', (req, res) => {
    const corpo = `Login: ${req.body.login}\nSenha:${req.body.senha}`; // recendo os dados da requisição POST.
    res.send('Access to Administration via POST:\n' + corpo); // exibindo os dados que foram enviados via requisição POST.
});

router.patch('/:id', (req, res) => {
    res.send('Access to Administration via PATCH');
});

router.put('/:id', (req, res) => {
    res.send('Access to Administration via PUT');
});

router.delete('/:id', (req, res) => {
    res.send('Access to Administration via DELETE');
});

module.exports = router;