/*
 * File: path.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 14:00:54
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 14:00:55
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const path = require('path');

//console.log('basename:', path.basename('c:\\temp\\arquivo.html')); // não funcionou corretamente no Linux.
console.log('basename:', path.basename('/temp/arquivo.html')); // funcionou corretamente no Linux
console.log('normalize:', path.normalize('/temp/dir//subdir/dir/../arquivo.html'));
console.log('join path:', path.join('/teste', 'teste2', 'teste3/teste4', 'dir2', '..'));
console.log('resolve:', path.resolve('path2.js')); // obs: ele não valida se o arquivo existe.
console.log('extension:', path.extname('path2.js')); // obs: ele não valida se o arquivo existe.
