/*
 * File: app.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Thursday, 14 May 2020 18:52:48
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Friday, 15 May 2020 20:20:45
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
const express = require('express');
const app = express();
const adminRoutes = require('./routes/admin');
const usuarioRoutes = require('./routes/usuario');
const cookieParser = require('cookie-parser');

app.use('/static', express.static('public')); // Diretório de arquivos estáticos.

app.use(express.json()); // habilita o uso de JSON nas requisições POST (função de middleware builtin - em nível de aplicação).
app.use(cookieParser()); // habilita a manipulação de cookies (função de middleware de terceiros interna do Express.JS).

app.use((req, res, next) => {
    console.log('Executando Middleware em nível de aplicação');
    return next();
});

app.get('/setcookie', (req, res) => {
    res.cookie('user', 'Glaucio Daniel', {max: 900000, httpOnly: true});
    return res.send('Cookie for gravado com sucesso.');
});

app.get('/getcookie', (req, res) => {
    let user = req.cookies['user'];
    if(user) {
        return res.send(user);
    }
});

app.get('/', (req, res) => {
    res.send('Hello World');
});

app.use('/admin', adminRoutes); // direciona todas as requisições para o arquivo de rotas (função de middleware personalizadas - em nível de rotas).
app.use('/usuarios', usuarioRoutes); // direciona todas as requisições para o arquivo de rotas (função de middleware personalizadas - em nível de rotas).

app.get('*', (req, res, next) => {
    setImmediate(() => {
        next(new Error('Temos um problema.'));
    });
});

app.use((err, req, res, next) => {                      // Função que irá "forçar" um erro.
    console.log('Geramos um erro, veja as instruções para corrigir');
    res.status(500).json({message: err.message});
});

app.listen(3000, () => {
    console.log('Server running at: http://localhost:3000/');
});