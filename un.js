/*
 * File: un.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 19:18:28
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 09 May 2020 19:18:28
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const fs = require("fs");

fs.readFile("file.txt", (err, data) => {
    if(err) throw err;
    console.log(data);
    fs.unlink("file.txt", (unlinkErr) => {
        if(unlinkErr) throw unlinkErr;
        console.log("Arquivo excluído com sucesso.");
    });
});

