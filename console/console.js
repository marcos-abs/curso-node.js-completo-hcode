/*
 * File: console.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 07:52:07
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 07:52:07
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

console.log('Exibindo uma mensagem no console');

// Erro

console.error(new Error('Exibindo mensagem de erro, temos problemas!'));

// em forma de tabelas

const carros = ['GM', 'FIAT', 'FORD', 'VW', 'Renault', 'Honda', 'Hyundai'];

console.table(carros);

const dados = { name: 'Gláucio Daniel', empresa: 'Hcode'};

console.table(dados);

console.count('processo');
console.count('processo');
console.count('processo');

console.log('Resetando o processo');

console.countReset('processo');
console.count('processo');


console.time('contador');

for (let index = 0; index < 100; index++) {
    console.log('-');
}

console.timeEnd('contador');

console.assert(true, 'Faça alguma coisa');

console.assert(false, 'ih rapaz %s, que pena!', 'Não funcionou');

console.clear();

