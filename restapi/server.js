/*
 * File: server.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Monday, 18 May 2020 17:55:52
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 18 May 2020 19:06:05
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
require('dotenv').config(); // necessário para carregar as informações do(s) arquivo(s) ".env".
const express = require('express');
const app = express();
const mongoose = require('mongoose');

mongoose.connect(process.env.DATABASE_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
const db = mongoose.connection;
db.on('error', (err) => {console.log('error:', err);});
db.once('open', () => {console.log('Database Connected.');});

app.use(express.json()); // necessário para que a API possa trabalhar com o padrão JSON.

const subscribersRouter = require('./routes/subscribers'); // caminho estático ainda, será substituído com "consign".
app.use('/subscribers', subscribersRouter);

app.listen(3000, () => {console.log('Servidor está rodando.');});
