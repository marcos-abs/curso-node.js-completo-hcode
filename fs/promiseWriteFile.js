/*
 * File: promiseWriteFile.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Monday, 11 May 2020 13:47:35
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 11 May 2020 13:47:35
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const {writeFile} = require('fs');

function criaArquivo(name, content) {
    return new Promise((resolve, reject) => {
        writeFile(name, content, err => {
            if(err) return reject(err);
            resolve();
        });
    });
}

criaArquivo('promiseArquivo.txt', 'Criando um arquivo utilizando promises')
    .then(() => {console.log('Arquivo promiseArquivo.txt criado com sucesso!');})
    .catch((err) => {console.log(err);});