/*
 * File: nodemon.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 21:56:50
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 11 May 2020 11:41:07
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const carro = 'Mercedes Benz';
console.log('carro: ', carro);
