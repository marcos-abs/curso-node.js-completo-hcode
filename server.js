/*
 * File: server.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 13:49:29
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Friday, 29 May 2020 09:39:47
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const http = require("http");
const host = "localhost";
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-type', 'text/plain');
    res.end('Ola Mundo!\nMeu primeiro script.');
});

server.listen (port, host, () =>  {
    console.log(`Server running at http://${host}:${port}`);
});
