/*
 * File: asyncRead.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Monday, 11 May 2020 13:57:28
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 11 May 2020 13:57:28
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const fs = require('fs').promises; //somente com a versão do Node.js acima da 12.0.0.

async function read() {
    const data = await fs.readFile('file.txt', 'binary');
    return new Buffer.from(data);
}

try {
    read().then(data => console.log(data.toString()));
} catch(error) {
    console.log(error);
}