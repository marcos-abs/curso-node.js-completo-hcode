/*
 * File: promise.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 22:10:49
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 09 May 2020 22:19:56
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

function soma(x) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve (x + 5000);
        }, 3000);
    });
}

soma(230).then((resultado) => {
    console.log('resultado: ', resultado);
});
