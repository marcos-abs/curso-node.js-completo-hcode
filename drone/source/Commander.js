/*
 * File: Commander.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 20:15:33
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Wednesday, 13 May 2020 18:57:20
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

class Commander {
    constructor(socket, host, port) {
        this.socket = socket;
        this.host = host;
        this.port = port;
    }

    sendInitCommand() {
        return new Promise((res, rej) => {
            this.socket.send('command', 0, 'command'.length, this.port, this.host, (err) => {
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }

    sendTakeOff() {
        return new Promise((res, rej) => {
            this.socket.send('takeoff', 0, 'takeoff'.length, this.port, this.host, (err) => {
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }

    sendLand() {
        return new Promise((res, rej) => {
            this.socket.send('land', 0, 'land'.length, this.port, this.host, (err) => {
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }

    sendForward(distance=20) {
        return new Promise((res, rej) => {
            this.socket.send(`forward ${distance}`, 0, `forward ${distance}`.length, this.port, this.host, (err) => {
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }

    sendRight(distance=20) {
        return new Promise((res, rej) => {
            this.socket.send(`right ${distance}`, 0, `right ${distance}`.length, this.port, this.host, (err) => {
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }

    sendLeft(distance=20) {
        return new Promise((res, rej) => {
            this.socket.send(`left ${distance}`, 0, `left ${distance}`.length, this.port, this.host, (err) => {
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }

    sendCw(distance=20) {
        return new Promise((res, rej) => {
            this.socket.send(`cw ${distance}`, 0, `cw ${distance}`.length, this.port, this.host, (err) => { // HACK: "distante" neste caso é igual a "graus"
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }

    sendCcw(distance=20) {
        return new Promise((res, rej) => {
            this.socket.send(`ccw ${distance}`, 0, `ccw ${distance}`.length, this.port, this.host, (err) => {  // HACK: "distante" neste caso é igual a "graus"
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }

    sendFlip() {
        return new Promise((res, rej) => {
            this.socket.send('flip b', 0, 'flip b'.length, this.port, this.host, (err) => {
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }

    sendBattery() {
        return new Promise((res, rej) => {
            this.socket.send('battery?', 0, 'battery?'.length, this.port, this.host, (err) => {
                if (err) {
                    return ref(err);
                } else {
                    return res();
                }
            });
        });
    }
}

module.exports = Commander;
