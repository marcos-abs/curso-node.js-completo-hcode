/*
 * File: childProcess.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 10:48:31
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 10:48:31
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const { spawn } = require('child_process');
const ls = spawn('ls', [ '..', '-lh', '/usr']);

ls.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
});


ls.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
});

ls.on('close', (code) => {
    console.log(`Processo em segundo pĺano finalizado com o código ${code}`);
});