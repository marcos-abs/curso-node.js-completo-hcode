/*
 * File: index.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Monday, 11 May 2020 11:45:37
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 11 May 2020 11:45:38
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const http = require('http');

const hostname = 'localhost';
const port = 3000;
const url = `http://${hostname}:${port}/`;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end('<h1>Bem Vindo<br>Node.JS</h1>');
});

server.listen(port, hostname, () => {
    console.log(`Servidor rodando em ${url}`);
});

const open = (process.platform == 'darwin' ? 'open' : process.platform == 'win32' ? 'start' : 'xdg-open');

require('child_process').exec(open + ' ' + url); // não funciona com o Quokka+Nodemon, e nem numa vm linux com Nodemon ... :(
