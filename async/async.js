/*
 * File: async.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 22:10:49
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 09 May 2020 22:32:54
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

function sum(x) {
    return new Promise((resolve, reject) => {
        if(isNaN(Number(x)) || Number(x) == undefined || typeof x != 'number') {
            reject(process.env.LOGNAME + ', tá de brincadeira!'); // USERNAME ==> Windows LOGNAME ==> Linux
        }
        setTimeout(() => {
            resolve (x + 5000);
        }, 3000);
    });
}

async function main() {
    try {
        const resultado = await sum('#');
        console.log('resultado: ', resultado);
    } catch (error) {
        console.log(`Temos problemas: ${error}`);
    }
}

main();
