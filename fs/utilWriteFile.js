/*
 * File: utilWriteFile.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Monday, 11 May 2020 13:17:55
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 11 May 2020 13:26:24
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const {promisify} = require('util');

const writeFile = promisify(require('fs').writeFile);
const conteudo = `Criando um arquivo utilizando promisify do módulo nativo uti.`;

writeFile('utilArquivo.txt', conteudo).then(() => {
    console.log('Arquivo utilArquivo criado com sucesso.');
}).catch((err) => {
    console.log(`Deu erro:, ${err}`);
});
