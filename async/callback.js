/*
 * File: callback.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 22:10:49
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 09 May 2020 22:10:49
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

function soma(x, callback) {
    return setTimeout(() => {
        return callback(null, x + 5000);
    }, 3000);
}

// function callback()
function resolveSoma(err, resultado) {
    if (err) throw err;
    console.log('resultado: ', resultado);
}

soma(200, resolveSoma);
