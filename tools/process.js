/*
 * File: process.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 23:04:12
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Sunday, 10 May 2020 00:05:04
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

require('./subdiretory/sub');
console.log('-------------------------------------------------------');
console.log('Nome do Arquivo: ', __filename);
console.log('Diretório do Arquivo: ', __dirname);
console.log('Conexão remota: ', process.env.SSH_CONNECTION);
console.log('Nome do usuário da conexão: ', process.env.USER);
console.log('Diretório atual: ', process.env.PWD);

switch(process.argv[2]) {
    case '-a':
        console.log('Execute a rotina principal');
        break;
    case '-i':
        console.log('Execute instalação');
        break;
    case '-q':
        console.log('Encerrando a aplicação');
        process.exit(); // interrompe imediatamente o código.
        break;
    default:
        console.log('Parâmetro inválido.');
}

console.log('Ambiente do Servidor: ', process.platform);
