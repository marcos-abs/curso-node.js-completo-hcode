/*
 * File: test.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 20:29:46
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 09 May 2020 20:29:46
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const crypto = require("crypto");
const start = Date.now();

function logHashTime() {
    crypto.pbkdf2("a", "b", 100000, 512, "sha512", () => {
        console.log(`Hash: ${Date.now() - start}`);
    });
}

logHashTime();
logHashTime();
logHashTime();
logHashTime();
logHashTime();