/*
 * File: errorEvent.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 14:26:44
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 14:26:44
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const { EventEmitter } = require('events');
const emitter = new EventEmitter();

const validaObjeto = (a) => {
    if(typeof a != 'object') {
        emitter.emit('error', new Error('Tipo informado inválido.'));
    } else {
        console.log('Objeto válido.');
    }
};

emitter.addListener('error', (err) => (
    console.log('Evento:' + err.message)
));

let dados = { name: 'Hcode', course: 'NodeJS' };

validaObjeto('dados');
validaObjeto(dados);
