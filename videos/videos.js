/*
 * File: videos.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 17:50:51
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 17:50:52
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const {spawn} = require('child_process');

const parent = process.argv[2];
let videos = [];

if(process.argv[2]) {
    const start = parseInt(process.argv[3]);
    const end = parseInt(process.argv[4]);
    for(let i = start; i <= end; i++) {
        videos.push(i);
    }
    videos.reverse();
    processVideo();
} else {
    console.log('Sintaxe: node videos pasta-de-origem sequencia-inicial sequencia-final');
}

async function processVideo() {
    let video = videos.pop();
    if (video) {
        try {
            await resize (video, 720);
            await resize (video, 480);
            await resize (video, 360);
            console.log(`Vídeos renderizados - ${video}`);
            processVideo();
        } catch(err) {
            console.log('Deu ruim...', err);
        }
    }
}

function resize(video, quality) {
    const p = new Promise((resolve, reject) => {
        const ffmpeg = spawn('/usr/bin/ffmpeg', [
            '-i',
            `${parent}/${video}.mp4`,
            '-codec:v',
            'libx264',
            '-profile:v',
            'main',
            '-preset',
            'slow',
            '-b:v',
            '400k',
            '-maxrate',
            '400k',
            '-bufsize',
            '800k',
            '-vf',
            `scale=-2:${quality}`,
            '-threads',
            '0',
            '-b:a',
            '128k',
            `${parent}/resultado/${video}-${quality}.mp4`
        ]);
        ffmpeg.stderr.on('data', (data) => {
            console.log(data);
        });
        ffmpeg.on('close', (code) => {
            resolve();
        });
    });
    return p;
}
