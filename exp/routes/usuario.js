/*
 * File: usuario.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Friday, 15 May 2020 18:53:08
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Friday, 15 May 2020 18:53:08
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const express = require('express');
const router = express.Router();

function logReq(req, res, next) {
    console.log('Executando a Função Middleware para a rota usuários');
    return next();
}

router.get('/', logReq, (req, res) => {
    res.send('Listando os usu&aacute;rios');
});

router.get('/:id', (req, res) => {
    res.send('Listando o usu&aacute;rio com o Id:' + req.params.id);
});

module.exports = router;