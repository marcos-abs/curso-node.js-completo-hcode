/*
 * File: fs-sync.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 18:29:18
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 09 May 2020 19:00:09
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const fs = require("fs");

console.log((process.hrtime()[0]/60).toFixed(5)); // em milissegundos.

console.log("Executando o console antes da leitura do arquivo");

const dados = fs.readFileSync("file.txt");

console.log("Executando o console após a leitura do arquivo");

console.log((process.hrtime()[0]/60).toFixed(5)); // em milissegundos.
