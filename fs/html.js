/*
 * File: html.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Monday, 11 May 2020 14:16:24
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Friday, 29 May 2020 09:42:11
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const http = require('http');
const {readFile} = require('fs');

const host = 'localhost';
const port = '3000';
const url = `http://${host}:${port}`;

let conteudo = '';

readFile('index.html', (err, data) => {
    if(err) throw err;
    conteudo = data;
});

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    // res.setHeader('Content-Type', 'text/plain');
    res.setHeader('Content-Type', 'text/html');
    res.end(conteudo);
});

server.listen(port, host, () => {
    console.log(`Servidor rodando! em ${url}`);
});

const start = (process.platform == 'dartwin' ? 'open' : process.platform == 'win32' ? 'start' : 'xdg-open');

require('child_process').exec(start + ' ' + url);
