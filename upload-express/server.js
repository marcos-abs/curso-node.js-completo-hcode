/*
 * File: server.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Saturday, 16 May 2020 22:57:14
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 16 May 2020 22:57:15
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const app = express();
const path = require('path');

app.use(bodyParser.urlencoded({extended:true}));

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/');
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

app.get('/', (req, res) => {
    // res.json({message: 'Bem vindo!'});
    res.sendFile(__dirname + '/index.html');
});

app.post('/upload', upload.single('arquivo'), (req, res, next) => {
    const file = req.file;
    if (!file) {
        const err = new Error('Por favor selecione um arquivo.');
        err.httpStatusCode = 400;
        return next(err);
    }
    res.send(file);
});

app.listen(3000, '192.168.56.107', () => {
    console.log('Server running on port 3000');
});
