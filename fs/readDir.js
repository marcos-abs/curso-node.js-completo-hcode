/*
 * File: readDir.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Monday, 11 May 2020 12:17:13
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 11 May 2020 12:17:14
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const fs = require('fs');

fs.readdir('../', (err, data) => {
    if(err) throw err;
    data.forEach((files) => {
        console.log(__dirname + '/' + files);
    });
});