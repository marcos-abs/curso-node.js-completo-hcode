/*
 * File: events.mjs
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 09 May 2020 22:53:04
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 17:10:51
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

import {EventEmitter} from 'events';

/**
 * para executar este modulo experimental é necessário utilizar a seguinte sintaxe:
 * $ node --experimental-modules events.mjs
 */

class Evento extends EventEmitter {}
const meuEvento = new Evento();

// subscriber - assinante
meuEvento.on('segurança', (x, y) => {
    console.log(`Executando o evento 'segurança': x=${x} y=${y}`);
});

// publisher - emissor
meuEvento.emit('segurança', 'userAdmin', 'Alterou Salário');

meuEvento.on('encerrar', (dados) => {
    console.log('Assinante: ' + dados);
});

meuEvento.emit('encerrar', 'Encerrando a execução da importação dos dados!');
