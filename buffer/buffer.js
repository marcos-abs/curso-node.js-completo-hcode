/*
 * File: buffer.js
 * Project: Curso de NodeJS Completo - Hcode
 * File Created: Tuesday, 12 May 2020 14:38:43
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 12 May 2020 14:38:43
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

const Buffer = require('buffer').Buffer;

const buf = Buffer.from('Curso Completo de NodeJS');

console.log('buf(1): ', buf);
console.log('buf(2): ', buf.toString());
console.log('buf(3): ', buf.toString('utf16le'));

const buf_string = Buffer.from('Carregando uma string', 'utf-8');

console.log('buf_string: ', buf_string);
console.log('buf_string: ', buf_string.toString());

console.log('is buffer?: ', Buffer.isBuffer(buf_string));

console.log('part of the buf_string: ', buf_string.toString('utf-8',0,10));
